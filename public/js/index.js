'use strict';
var spherum = {};
SmoothScroll({ stepSize: 30 });

;(function ($, window, document, undefined) {

    //SVG Fallback
    if(!Modernizr.svg) {
        $("img[src*='svg']").attr("src", function() {
            return $(this).attr("src").replace(".svg", ".png");
        });
    };



    /* ==================================================
     /* инпуты
     /* ==================================================*/
    spherum.inputs = function () {

        var $input, $field;

        $input = $('.input');
        $field = $input.find('input'); 
        
        if ( $input.find('.input__label').length )
            $input.addClass('input_has-label');

        $field
            .on({
                focus: function() {
                    if ( !$(this).parents('.input').hasClass('input_focused') ){
                        $(this).parents('.input').removeClass('input_hovered');
                        $(this).parents('.input').addClass('input_focused');
                    }
                },
                blur: function() {
                    if ( $(this).parents('.input').hasClass('input_focused') && $(this).val() == '' )
                        $(this).parents('.input').removeClass('input_focused');
                },
                mouseenter: function () {
                    if ( !$(this).parents('.input').hasClass('input_focused') && !$(this).parents('.input').hasClass('input_hovered') )
                        $(this).parents('.input').addClass('input_hovered');
                },
                mouseleave: function () {
                    if ( !$(this).parents('.input').hasClass('input_focused')  && $(this).parents('.input').hasClass('input_hovered') )
                        $(this).parents('.input').removeClass('input_hovered');
                }
            });


    };


    /* ==================================================
     /* carousel
     /* ==================================================*/
    spherum.carousel = function () {
        var owl = $("#owl");

        owl.owlCarousel({
            pagination : false,
            autoPlay: 6000,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true
        });



        // Custom Navigation Events
        $(".next").click(function(){
            owl.trigger("owl.next");
        });
        $(".prev").click(function(){
            owl.trigger('owl.prev');
        });

    };

    /* ==================================================
     /* topnav
     /* ==================================================*/
    spherum.topnav = function () {
        $('.js-scrollto').on('click', function() {

            var scrollAnchor = $(this).attr('data-scroll'),
                scrollPoint = $('section[data-anchor="' + scrollAnchor + '"]').offset().top + 50;

            $('body,html').animate({
                scrollTop: scrollPoint
            }, 500);

            return false;

        });
    };

    /* ==================================================
     /* modal
     /* ==================================================*/
    spherum.modal = function () {
            $('.popup').magnificPopup({
                type: 'inline',
                preloader: false,
                focus: '#name',
                removalDelay: 500, //delay removal by X to allow out-animation

                // When elemened is focused, some mobile browsers in some cases zoom in
                // It looks not nice, so we disable it:
                callbacks: {
                    beforeOpen: function() {
                        this.st.mainClass = this.st.el.attr('data-effect');
                        $('html').addClass(this.st.el.attr('data-effect'));
                        if($(window).width() < 700) {
                            this.st.focus = false;
                        } else {
                            this.st.focus = '#name';
                        }
                    },
                    beforeClose: function() {
                        $('html').removeClass(this.st.el.attr('data-effect'));
                    }
                },
                midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
            });
    }




})(jQuery, window, document);



$(document).ready(function() {
    spherum.inputs();
    spherum.carousel();
    spherum.topnav();
    spherum.modal();
});